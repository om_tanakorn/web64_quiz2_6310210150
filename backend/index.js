/*Token no admin 
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlRoZWxlZSIsInJldmlld2VyX2lkIjoxLCJJc0FkbWluIjowLCJpYXQiOjE2NDc2ODE3MjcsImV4cCI6MTY0Nzc2ODEyN30.ZoCK99SKFsNxI3-HlsLpWdpHdHGDf5W2DRqmLVSA6-o 
*/

/*Token had admin
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluMSIsInJldmlld2VyX2lkIjoyLCJJc0FkbWluIjoxLCJpYXQiOjE2NDc2ODE4MDQsImV4cCI6MTY0Nzc2ODIwNH0.to2eTVKZW4_xBnGLdC7dlgcMuB1gq5tb94BZ9mCQYZc 
*/

const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'coffeeadmin',
    password : 'coffeeadmin',
    database : 'CoffeeReview'
})

connection.connect();
const express =require('express')
const jsonwebtoken = require('jsonwebtoken')
const app = express()
const port = 4000

/* ===================== Middlewar ================= */

function autheticateToken(req , res , next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null ) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET, (err,user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}
/* ================================================== */

/* ========== CRDUD Operation for Reviewer ========== */

/* === LOGIN === */
app.get("/login" , (req,res) => {
    let username = req.query.username
    let password = req.query.password
    let query = `SELECT * FROM Reviewer WHERE Username = '${username}'`

    connection.query(query ,(err,row) => {
        if(err){
            console.log(err)
            res.json({
                "STATUS" : "400" ,
                "MESSAGE" : "Error noting in Databesssssssss "
            })
        }else{
            let db_password = row[0].Password
            bcrypt.compare(password, db_password , (err,result) => {
                if(result){
                    let payload = {
                        "username" : row[0].Username,
                        "reviewer_id" : row[0].ReviewerID,
                        "IsAdmin" : row[0].isAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload , TOKEN_SECRET , {expiresIn : '1d'})
                    res.send(token)
                }else {res.send("Inalid usernaem / password")}
            })
        }
    })
})

/* === Register === */
app.get("/register" , (req,res) => {
    let reviewer_aka = req.query.reviewer_aka
    let reviewer_name = req.query.reviewer_name
    let reviewer_surname = req.query.reviewer_surname
    let username = req.query.username
    let password = req.query.password

    /* = bcrypt = */
    bcrypt.hash(password,SALT_ROUNDS,(err,hash) => {
        let query = `INSERT INTO Reviewer
                    (ReviewerAKA,ReviewerName,ReviewerSurname,Username,Password,isAdmin)
                    VALUES ('${reviewer_aka}','${reviewer_name}','${reviewer_surname}',
                            '${username}','${hash}',false )`
        
        console.log(query)
        connection.query(query,(err,rows) => {
            console.log(err)
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Add new user succesful"
            })
        }
        })
    })
})


/* ========== CRDUD Operation for Coffee ========== */

/* === LIST COFFEE === */
app.get("/list_coffee",(req,res) => {
    let query = `SELECT * from Coffee`
    connection.query(query,(err,rows) => {
        if(err){
            res.json({
                "STATUS" : "400",
                "MESSAGE" : "Error querying from coffee db"
            })
        }else{
            res.json(rows)
        }
    })
})


/* === ADD COFFEE === */
app.post("/add_coffee", autheticateToken ,(req,res) => {
    let coffee_name = req.query.coffee_name
    let coffee_price = req.query.coffee_price

    if(!req.user.IsAdmin) res.send("You not Admin")

    let query = `INSERT INTO    Coffee (CoffeeName,CoffeePrice)
                                VALUES ('${coffee_name}','${coffee_price}' )`

    connection.query(query,(err,rows) =>{
        if(err){
            res.json({
                "STATUS" : "400",
                "MASSGE" : "Error can't add your coffee"
            })
        }else{
            res.json({
                "STATUS" : "200",
                "MASSGE" : `ADD '${coffee_name}' succesful`
            })
        }
    })

})

/* === UPDATE COFFEE === */
app.post("/update_coffee" , autheticateToken ,(req,res) => {
    let coffee_id = req.query.coffee_id
    let coffee_name = req.query.coffee_name
    let coffee_price = req.query.coffee_price

    if(!req.user.IsAdmin) res.send("You not Admin")
    
    let query = `UPDATE Coffee SET
                    CoffeeName = '${coffee_name}',
                    CoffeePrice = ${coffee_price}
                    WHERE CoffeeID = ${coffee_id}`

    console.log(query)

    connection.query(query,(err,rows) =>{
        console.log(err)
        if (err){
            res.json({
                "STATUS" : "400",
                "MESSAGE" : "ERROR can't update coffee"
            })
        }else{
            res.json({
                "STATUS" : "200",
                "MESSAGE" : `Updating ${coffee_id} succesful`
            })
        }
    })
})

/* === DELETE COFFEE === */
app.post("/delete_coffee" , autheticateToken ,(req,res) => {

    let coffee_id = req.query.coffee_id

    if(!req.user.IsAdmin) res.send("You not Admin")

    let query = `DELETE FROM Coffee WHERE CoffeeID=${coffee_id}`

    console.log(query)
    connection.query(query,(err,row) => {
        if(err){
            res.json({
                "STATUS" : "400",
                "MESSAGE" : "ERROR can't update coffee"
            })
        }else{
            res.json({
                "STATUS" : "200",
                "MESSAGE" : `Delete ${coffee_id} succesful`
            })
        }
    })
})


app.listen(port,() =>{
    console.log(`Now starting Runing System Backend ${port} `)
})